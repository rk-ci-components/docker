# [1.3.0](https://gitlab.com/rk-ci-components/docker/compare/1.2.5...1.3.0) (2024-04-06)


### Features

* upgraded docker job ([6aae04e](https://gitlab.com/rk-ci-components/docker/commit/6aae04e27e24529182adddaa1a1f9de4a72468e7))

## [1.2.5](https://gitlab.com/rk-ci-components/docker/compare/1.2.4...1.2.5) (2024-04-02)


### Bug Fixes

* missing $Dockerfile ([400a024](https://gitlab.com/rk-ci-components/docker/commit/400a0247019f9195805662ab37c51816ceee0d72))

## [1.2.4](https://gitlab.com/rk-ci-components/docker/compare/1.2.3...1.2.4) (2024-04-02)


### Bug Fixes

* run trivy only on prod ([e31969e](https://gitlab.com/rk-ci-components/docker/commit/e31969e7ab0d3e8d1aae82cebf03123fae36924d))

## [1.2.3](https://gitlab.com/rk-ci-components/docker/compare/1.2.2...1.2.3) (2024-04-02)


### Bug Fixes

* **trivy:** fix needs ([2e8b446](https://gitlab.com/rk-ci-components/docker/commit/2e8b44635ede5845b8d4a1765bbb5f5a70bf071d))
* **validation:** fix validation of gitlab-ci files ([604f431](https://gitlab.com/rk-ci-components/docker/commit/604f4313272c46aaed6be647072e9393adc349d6))

## [1.2.2](https://gitlab.com/rk-ci-components/docker/compare/1.2.1...1.2.2) (2024-04-02)


### Bug Fixes

* **trivy:** only run trivy when build completes ([94baf70](https://gitlab.com/rk-ci-components/docker/commit/94baf70c247067338cf497478d4e54f9126efe62))

## [1.2.1](https://gitlab.com/rk-ci-components/docker/compare/1.2.0...1.2.1) (2024-04-02)


### Bug Fixes

* **variables:** fix TRIVY_DISABLED variable identifier ([d59a22e](https://gitlab.com/rk-ci-components/docker/commit/d59a22e5f9c166ede18fbd44a8e16aaf24cd1ee7))

# [1.2.0](https://gitlab.com/rk-ci-components/docker/compare/1.1.0...1.2.0) (2024-04-01)


### Features

* **security:** added trivy job ([c854d1d](https://gitlab.com/rk-ci-components/docker/commit/c854d1df941aa9fc9a2cb46b7b0dfba26b78de14))

# [1.1.0](https://gitlab.com/rk-ci-components/docker/compare/1.0.0...1.1.0) (2024-04-01)


### Features

* **code-quality:** added hadolint job ([2de5095](https://gitlab.com/rk-ci-components/docker/commit/2de50952177e2628e339aab30d6fb4fb8d88ffdb))

# 1.0.0 (2024-04-01)


### Features

* initial commit ([2baf268](https://gitlab.com/rk-ci-components/docker/commit/2baf2689d5f98faf5823f0e2692089fde537b574))

## [1.0.2](https://gitlab.com/rk-ci-components/semantic-release/compare/1.0.1...1.0.2) (2024-04-01)


### Bug Fixes

* **changelog:** fix changelog creation ([82927e7](https://gitlab.com/rk-ci-components/semantic-release/commit/82927e765752012d26a17f30712fa63c566baf03))

## [1.0.1](https://gitlab.com/rk-ci-components/semantic-release/compare/v1.0.0...1.0.1) (2024-04-01)


### Bug Fixes

* **changelog:** fix changelog creation ([49bdf28](https://gitlab.com/rk-ci-components/semantic-release/commit/49bdf28ebb0d56da15609d5e083766fd3b49eb87))

# 1.0.0 (2024-04-01)


### Features

* **initial:** initial release ([2c1f2cd](https://gitlab.com/rk-ci-components/semantic-release/commit/2c1f2cd66b30fe0316e5fc9a68d782a8d076f284))
