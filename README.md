# GitLab CI component for Docker

[![Latest Release](https://gitlab.com/rk-ci-components/docker/-/badges/release.svg)](https://gitlab.com/rk-ci-components/docker/-/releases)
[![pipeline status](https://gitlab.com/rk-ci-components/docker/badges/master/pipeline.svg)](https://gitlab.com/rk-ci-components/docker/-/commits/master)

This project implements a GitLab CI/CD template to build, check and inspect your containers with [Docker](https://www.docker.com/).

## Usage

This template can be used as a [CI/CD component](https://docs.gitlab.com/ee/ci/components/#use-a-component-in-a-cicd-configuration).

### Use as a CI/CD component

Add the following to your `gitlab-ci.yml`:

```yaml
include:
  - component: gitlab.com/rk-ci-components/docker/docker@master
    inputs:
      dockerfile: "docker/Dockerfile"
```

## Global configuration

The semantic-release template uses some global configuration used throughout all jobs.

| Input / Variable                          | Description                   | Default value |
|-------------------------------------------|-------------------------------|---------------|
| `dockerfile` / `DOCKERFILE`               | The path to your `Dockerfile` | `Dockerfile`  |
| `hadolint-disabled` / `HADOLINT_DISABLED` | Disables the Hadolint job.    | `false`       |
| `trivy-disabled` / `TRIVY_DISABLED`       | Disables the Trivy job.       | `false`       |
| `build-on-mr` / `BUILD_ON_MR`             | Build preview build on merge request event       | `false`       |

## Jobs

### `docker-hadolint` job

This job performs a [Lint](https://github.com/hadolint/hadolint) on your `Dockerfile`.

It is bound to the `test` stage, and is **enabled by default**.

### `docker-build` job

This job builds the Dockerfile and publishes it to Gitlab's container registry.

It is bound to the `package-build` stage.

### `docker-trivy` job

This job performs a Vulnerability Static Analysis with [Trivy](https://github.com/aquasecurity/trivy) on your built image.

It is bound to the `package-test` stage, and is **enabled by default**, but will only run when `docker-build` succeeds.