spec:
  inputs:
    dockerfile:
      description: 'Path to your Dockerfile'
      default: 'Dockerfile123'
    hadolint-disabled:
      description: Disable Hadolint
      type: boolean
      default: false
    trivy-disabled:
      description: Disable Trivy
      type: boolean
      default: false
    build-on-mr:
      description: Trigger a build on merge request events, with the branch name as a tag
      type: boolean
      default: false
---
include:
  - component: gitlab.com/rk-ci-components/core/core@master

variables:
  DOCKERFILE: $[[ inputs.dockerfile ]]
  HADOLINT_DISABLED: $[[ inputs.hadolint-disabled ]]
  TRIVY_DISABLED: $[[ inputs.trivy-disabled ]]
  BUILD_ON_MR: $[[ inputs.build-on-mr ]]

.docker-scripts: &docker-scripts |
  # BEGSCRIPT
  set -e
  
  function docker_tag_and_push() {
    tag=$1

    log_info "Tagging Docker image with `$tag`..."
    docker tag $CI_REGISTRY_IMAGE:temp $CI_REGISTRY_IMAGE:$tag
    docker push $CI_REGISTRY_IMAGE:$tag
  }
  
  # ENDSCRIPT

docker-hadolint:
  image: hadolint/hadolint:latest-debian
  stage: test
  before_script:
    - mkdir -p -m 777 reports
  script:
    - hadolint -f gitlab_codeclimate $DOCKERFILE > reports/hadolint-$(md5sum $DOCKERFILE | cut -d" " -f1).json
  artifacts:
    name: "$CI_JOB_NAME artifacts from $CI_PROJECT_NAME on $CI_COMMIT_REF_SLUG"
    expire_in: 1 day
    when: always
    reports:
      codequality:
        - "reports/*"
    paths:
      - "reports/*"
  rules:
    - if: '$HADOLINT_DISABLED == "true"'
      when: never
    - !reference [ .test-policy, rules ]

docker-build:
  image: docker:20.10.16
  services:
    - docker:20.10.16-dind
  stage: package-build
  before_script:
    - !reference [.core-scripts]
    - !reference [.docker-scripts]
    - export PROD_REF=${PROD_REF%/}
    - export PROD_REF=${PROD_REF#/}
    - export INTEG_REF=${INTEG_REF%/}
    - export INTEG_REF=${INTEG_REF#/}
    - echo "Logging in to Gitlab registry"
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - log_info "Building docker image"
    - docker build -t $CI_REGISTRY_IMAGE:temp -f $DOCKERFILE --target production .
    # Tag MR image
    - |
      if [ $CI_MERGE_REQUEST_ID != null ] && [ $BUILD_ON_MR == "true" ]; then
        docker_tag_and_push "$CI_COMMIT_REF_SLUG"
      fi
    # Tag integration image
    - |
      if [[ $CI_COMMIT_REF_NAME =~ $INTEG_REF ]]; then
        docker_tag_and_push "integration"
      fi
    # Tag production image
    - |
      if [[ $CI_COMMIT_REF_NAME =~ $PROD_REF ]]; then
        docker_tag_and_push "latest"
      fi
  rules:
    - if: $CI_COMMIT_REF_NAME =~ $PROD_REF || $CI_COMMIT_REF_NAME =~ $INTEG_REF
      when: always
    - if: '$BUILD_ON_MR == "true" && $CI_PIPELINE_SOURCE == "merge_request_event"'
      when: always
    - when: never

docker-trivy:
  image: aquasec/trivy
  stage: package-test
  needs: ["docker-build"]
  before_script:
    - mkdir -p -m 777 reports
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker pull $IMAGE_TAG
  script:
    - trivy --exit-code 0 --cache-dir .trivycache/ --no-progress --format template --template "@contrib/gitlab.tpl" -o reports/gl-container-scanning-report.json $IMAGE_TAG
  artifacts:
    name: "$CI_JOB_NAME artifacts from $CI_PROJECT_NAME on $CI_COMMIT_REF_SLUG"
    expire_in: 1 day
    when: always
    reports:
      container_scanning:
        - "reports/*"
    paths:
      - "reports/*"
  cache:
    paths:
      - .trivycache/
  rules:
    - if: '$TRIVY_DISABLED == "true"'
      when: never
    - if: $CI_COMMIT_REF_NAME == $PROD_REF
      when: always